class Flatten

  def flatten(array)
    out = []
    rec(array, out)
  end

  private
  def rec(element, out)
    if element.class == Array
      element.each do |e|
        rec(e, out)
      end
    else
      out << element
    end
    out
  end

end
