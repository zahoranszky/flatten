require_relative '../lib/flatten'

describe Flatten do
   context "When testing the Flatten class" do

      it "should say [], [1], [1, 2], [1, [2, [3, 4]]] when we call the flatten method" do
         expect(Flatten.new.flatten([])).to eq []
         expect(Flatten.new.flatten([1])).to eq [1]
         expect(Flatten.new.flatten([1, 2])).to eq [1, 2]
         expect(Flatten.new.flatten([1, [2, [3, 4]]])).to eq [1, 2, 3, 4]
      end

   end
end
